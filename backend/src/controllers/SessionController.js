//metodos dentro de um controller index, show, store, upadate, destroy
//index = listagem de secoes
//show = listar uma unica secao
//store = criar uma secao 
//upadate = para editar uma sacao
//destroy = para deletar uma secao

const User = require('../models/User');

module.exports = {
    async store(req, res) {
        const  { email } = req.body;

        let user = await User.findOne({ email });

        if(!user){
            const user = await User.create({ email });
        }


        return res.json(user);
    }
};