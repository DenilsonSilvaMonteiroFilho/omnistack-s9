const express = require('express');
const mongoose = require ('mongoose');

const routes = require ('./routes');

const app = express();

mongoose.connect('mongodb+srv://usuarioNormal:usuarioNormal@omnistackcp-x2npp.mongodb.net/teste?retryWrites=true&w=majority',{
    useNewUrlParser:true,
    useUnifiedTopology: true,
})

//GET, POST, PUT, DELETE Metodos em uma API req 
//GET = Para aplicar funcoes do backend, POST = Criar novas informacoes no backend
//PUT = Quando quer editar alguma informacao , DELETE = Para deleter informacoes

//req.query = Acessar query params (para filtro)
//req.params = Acessar route params (para edicao e delete)
//req.body = Acessar corpo da requisicao (para criacao e edicao)

app.use(express.json());
app.use(routes);


app.listen(3333);
